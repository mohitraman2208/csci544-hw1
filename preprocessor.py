'''
Created on Jan 24, 2015

@author: Mohit
'''

import sys
import os

#Take directory containing training data from arguments /home/mohit/PycharmProjects/untitled/SPAM_training
training_dir_path = sys.argv[1]

training_output_filename = sys.argv[2]

mode = int(sys.argv[3]) # mode 1 - training, 2 - testing

dirname = os.path.abspath(training_dir_path)
if dirname == '' or not os.path.isdir(dirname):
    print("Invalid Directory Name")
    sys.exit(0)

if mode == 2:
    f_out = open("output_sentiment.txt", "w")

# open output file
with open(training_output_filename,"w") as output_file:

    # fetch all files
    myfiles = [ f for f in os.listdir(training_dir_path) if os.path.isfile(os.path.join(training_dir_path, f))]
    myfiles.sort()
    for filename in myfiles:
        label = filename.split('.')[0] 
        with open(os.path.join(training_dir_path,filename),'r',encoding='utf-8',errors='ignore') as file_content:
            if mode == 1:
                output_file.write(label + " " +" ".join(file_content.read().splitlines()))
            elif mode == 2:
                output_file.write(" ".join(file_content.read().splitlines()))
                f_out.write(label+"\n")
            output_file.write("\n")

if mode == 2:
    f_out.close()


