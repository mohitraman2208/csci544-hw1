'''
Created on Jan 24, 2015

@author: Mohit
'''
import json
import sys
import os.path
import re
from math import log

TOTAL_WORD_COUNT_CONST = "_TotalWordCount_"
TOTAL_MSG_COUNT_CONST = "_TotalMSGCount_"
PRIOR_PROB_CONST = "_PriorProB_"
VOCAB_CONST = "_Vocab_"


def init_dict_label(my_label,my_dict):
    my_word_dict = {TOTAL_WORD_COUNT_CONST: 0, TOTAL_MSG_COUNT_CONST: 0}
    my_dict[my_label] = my_word_dict


# Command line argument 1 - Training file name
train_file_name = sys.argv[1]

# Command line argument 2 - Model file name
model_file_name = sys.argv[2]

if train_file_name == '' or not os.path.isfile(os.path.abspath(train_file_name)) or model_file_name == '':
    print("Invalid Arguments!")

# init
my_label_dict = {}  # ex: my_label_dict = {'LABEL1' :{'TotalWordCount' : 100,'word1':'',..}}
totalMsgCount = 0
word_set = []
with open(os.path.abspath(train_file_name), 'r', errors='ignore') as train_file_handle: # with open(os.path.abspath('tst_input.txt'),'r',errors='ignore') as train_file_handle:
    for line in train_file_handle:

        totalMsgCount += 1

        # Remove special characters and get all words - filter 1
        #rx = re.compile()
       # wordList = re.sub(r"([^\w])", r" \1 ",  line).split()
        wordList = line.split()

        # First word is label of text line
        label = wordList[0]

        if label not in my_label_dict:
            init_dict_label(label, my_label_dict)

        word_dict_local = my_label_dict[label]
        word_dict_local[TOTAL_WORD_COUNT_CONST] += len(wordList) - 1
        word_dict_local[TOTAL_MSG_COUNT_CONST] += 1

        for word in wordList[1:]:

            #word = word.lower()
            #if word == "subject":
             #   continue
            word_set.append(word)
            # TODO filter 2

            word_dict_local[word] = word_dict_local[word] + 1 if word in word_dict_local else 1

total_word_count = len(set(word_set))
for label, word_dict in my_label_dict.items():
    word_dict[PRIOR_PROB_CONST] = log(word_dict[TOTAL_MSG_COUNT_CONST]/totalMsgCount)
    word_dict[VOCAB_CONST] = total_word_count

with open(model_file_name,'w') as output_file:
    output_file.write(json.dumps(my_label_dict,indent=1))

