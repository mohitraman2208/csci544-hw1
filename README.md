# README #

#Naive Bayes Classifier#

### Instructions to run the code: ###

Creating training and testing file:

Training file -

python3 preprocessor.py training_dir_name training_file_name 1 

Testing file - 

python3 preprocessor.py testing_dir_name testing_file_name 2 

Supervised Learning using training data:

python3 nblearn.py training_file_name model_file_name

Classify Test Data(Prints to STDOUT):

python3 nbclassify.nb model_file_name testing_file_name  

Evaluate classifier: 

python3 evalclasifier.py actual_file_name output_from_classifier
 
# Part3 #

Fscore/Precision/Recall for my classifiers and off-the-shelf SVM and MegaM classifiers on same data/dev sets- 

##My Classifier - ##

Spam Data Set -

HAM:

 Fscore:0.9859437751004014

precision:0.982

Recall:0.9899193548387096

SPAM:

 Fscore:0.9618528610354223

precision:0.9724517906336089

Recall:0.9514824797843666

Accuracy:0.979457079970653

Sentiment Data Set -

NEG:

 Fscore:0.858424336973479

precision:0.8892929292929292

Recall:0.8296268375424048

POS:

 Fscore:0.8080380750925437

precision:0.7717171717171717

Recall:0.8479467258601554

Accuracy:0.837037037037037


##MegaM  - ##
Sentiment Data Set - 

POS:

Fscore:0.8744326777609681

precisiom:0.8731117824773413

Recall:0.8757575757575757

NEG:

Fscore:0.899271844660194

precision:0.9003645200486027

Recall:0.8981818181818182

Accuracy:0.8882154882154882

Spam Data Set - 

HAM:

Fscore:0.9904857285928894

precision:0.9919759277833501

Recall:0.989

SPAM:

Fscore:0.9739368998628258

precision:0.9699453551912568

Recall:0.977961432506887

##SVM##

SPAM Data Set -

SPAM:

F-score:0.8674698795180723

precision:0.9568106312292359

Recall:0.7933884297520661

HAM:

F-score:0.9573229873908826

precision:0.9293785310734464

Recall:0.987

Sentiment Data Set - 

NEG:

F-score:0.8738289814630258

precision:0.8623131392604249

Recall:0.8856565656565657

POS:

F-score:0.8374004623683534

precision:0.8520648196549921

Recall:0.8232323232323232

What happens exactly to precision, recall and F-score in each of the two tasks (on the development data) when only 10% of the training data is used to train the classifiers in part I and part II?

Naive Bayes - We see F-score,precision and recall reduce by 4-5% on sentiment data set. This is obvious as sentiment classification is a hard problem to solve for Naive Bayes in itself and less data has less features which means our classifier is not able to learn some of the features required to classify more test data correctly.

We do not see much change in the values, this suggests that most of the critical features can be found from small set of data.

There is just 0-2% change in Spam data set. This clearly suggests that SPAM data can be more easily learned as there is less relationship between words like ( not bad ) as compared to sentiment data set.

SVM/MegaM - We see  drastic drop in F-score of off-shelf classifiers. This suggests that they need more data to find support for their features and use only those features which has a good support value. This is just a guess.


##Contact Info##
mohitram@usc.edu

6692258548