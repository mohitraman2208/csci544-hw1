'''
Created on Jan 24, 2015

@author: Mohit
'''
import sys
import json
import re
from math import log

TOTAL_WORD_COUNT_CONST = "_TotalWordCount_"
TOTAL_MSG_COUNT_CONST = "_TotalMSGCount_"
PRIOR_PROB_CONST = "_PriorProB_"
VOCAB_CONST = "_Vocab_"

mode_file_name = sys.argv[1]

# Load JSON data from Model file
try:
    with open(mode_file_name,"r") as mode_file_handle:
        my_label_dict = json.loads(mode_file_handle.read())
except IOError:
    print("Error: Cannot open File")
    sys.exit(1)
except TypeError:
    print("Error: Invalid JSON Model File")
    sys.exit(1)

test_file_name = sys.argv[2]


try:
    #f_out = open("output.txt","w")

    with open(test_file_name,"r",errors="ignore") as test_file_handle:
        for line in test_file_handle:

            # Remove special characters and get all words - filter 1
            #wordList = re.sub("[^\w]", " ",  line).split()
            wordList = line.split()
            #wordList = re.sub(r"([^\w])", r" \1 ",  line).split()

            max_prob = float("-inf");
            max_label = ""
            for label, word_dict in my_label_dict.items():

                prior_prob = word_dict[PRIOR_PROB_CONST]

                count_prob = 0

                # Add 1 Smoothing - total + k(total unique words)
                total_words = word_dict[TOTAL_WORD_COUNT_CONST] + word_dict[VOCAB_CONST] + 1

                for word in wordList:
                    # word = word.lower()
                    count_word = word_dict[word] + 1 if word in word_dict else 1
                    count_prob += log(float(count_word/total_words))

                total_prob = prior_prob + count_prob

                if total_prob > max_prob:
                    max_label = label
                    max_prob = total_prob

            print(max_label)
     #       f_out.write(max_label + "\n")
    #f_out.close()
except IOError:
    print("Cannot open Test file")


